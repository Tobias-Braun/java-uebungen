# **Info** #

### *Zu findender Code:* ###

* Übungen aus Programmierung
* Miniprojekte aus Programmierung

### *Zu beachten:* ###

* Es kann nicht gewährleistet werden, dass der hier zur Verfügung gestellte Code fehlerfrei und/oder richtig/gut programmiert ist.
* Auch wird nicht garantiert, dass jedes Projekt online gestellt wird.